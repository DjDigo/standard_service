<?php
/**
 * The sidebar containing the main widget area
 * Template Name:about-us
 *
 * @package WordPress
 * @subpackage standard_service
 * @since standard_service 1.0
 */

get_header();
?>

<main class="main-content">
  <section class="banner banner--about-us">
    <div class="banner__content">
      <h2 class="banner__headline">About us</h2>
    </div>
  </section>

  <section class="section history">
    <div class="section__content">
      <?php
         $query_arg = array(
            'category_name'      => 'about-us',
            'post_status'    => 'publish',
          );

        $query = new WP_Query($query_arg);
       ?>

      <?php foreach ($query->posts as $key => $value): ?>

      <div class="section__title section__title--about-us">
        <h2><?= $value->post_title; ?></h2>
      </div>

      <div class="history__content">
        <?= $value->post_content; ?>
      </div>
      <?php endforeach; ?>

      <?php
         $query_acc = array(
            'category_name'      => 'about-acc-title',
            'post_status'    => 'publish',
          );

        $query_accordion = new WP_Query($query_acc);
       ?>

      <?php foreach ($query_accordion->posts as $key => $value): ?>
      <div class="section__title section__title--second section__title--about-us">
        <h2><?= $value->post_title; ?></h2>
      </div>
      
      <div class="history__content">
        <div class="history__detail history__detail--second">
          <div class="history__image-container history__image-container--left">
            <?= $value->post_content; ?>
          </div>
          
          <?php endforeach ?>

          <div class="history__detail-content history__detail-content--right">
          
          <?php
             $query_accordion = array(
                'category_name'      => 'about-us-accordion',
                'post_status'    => 'publish',
              );

            $query = new WP_Query($query_accordion);
           ?>
          <?php foreach ($query->posts as $key => $value): ?>
            <div class="history__accordion">
              <div class="history__title">
                <span class="history__title-content"><?= $value->post_title; ?></span>
                <span class="history__arrow history__arrow--open"></span>
              </div>
              <div class="history__accordion-content">
               <?= $value->post_content; ?>
              </div>
            </div>
            <?php endforeach ?>           
            <div class="history__readmore">
              <a href="#" class="history__readmore-text">Read more</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>    

  <section class="chart">
    <div class="chart__content">
      <div class="chart__information">
        <div class="chart__title">
          <h2>Does you reputation Effecting your Business ?</h2>
        </div>
        <div class="chart__paragraph">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="chart__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
  </section>


  <section class="section blogs">
    <div class="section__content">
      <div class="section__title section__title--blogs">
        <h2>Latest Blog</h2>
      </div>  

      <div class="blogs__list-container">
        <ul>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog1.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog2.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog3.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog4.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>


</main>
<?php get_footer(); ?>

