<?php
/**
 * The sidebar containing the main widget area
 * Template Name:blog
 *
 * @package WordPress
 * @subpackage standard_service
 * @since standard_service 1.0
 */

get_header();
?>

<main class="main-content">
  <section class="banner banner--blog">
    <div class="banner__content">
      <h2 class="banner__headline">Blog</h2>
    </div>
  </section>

  <section class="section">
    <div class="section__content section__content--panel">
      <div class="panel">
        <div class="panel__blog-list">
          <div class="panel__blog-title">
            <h2 class="panel__title-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h2>
          </div>
          <div class="panel__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog-page.jpg" class="panel__blog-image">
          </div>
          <div class="panel__special-icons">
            <ul>
              <li class="panel__special-icon-list">
                <i class="fa fa-calendar panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">November 19, 2017</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-align-left panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Blog, Images, Blog, Posts</span
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-user panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Admin</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-comments panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">1 Comments</span>
              </li>
            </ul>
          </div>
          <div class="panel__blog-paragraph">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimectetur adipisicing elit, sed ectetur adipisicing elit, sed </p>
          </div>

          <div class="panel__button">
            <button class="button button--readmore"><span class="panel__button-readmore">READ MORE</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
          </div>
        </div>
        
        <div class="panel__blog-list">
          <div class="panel__blog-title">
            <h2 class="panel__title-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h2>
          </div>
          <div class="panel__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog-page2.jpg" class="panel__blog-image">
          </div>
          <div class="panel__special-icons">
            <ul>
              <li class="panel__special-icon-list">
                <i class="fa fa-calendar panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">November 19, 2017</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-align-left panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Blog, Images, Blog, Posts</span
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-user panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Admin</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-comments panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">1 Comments</span>
              </li>
            </ul>
          </div>
          <div class="panel__blog-paragraph">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimectetur adipisicing elit, sed ectetur adipisicing elit, sed </p>
          </div>

          <div class="panel__button">
            <button class="button button--readmore"><span class="panel__button-readmore">READ MORE</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
          </div>
        </div>

        <div class="panel__blog-list">
          <div class="panel__blog-title">
            <h2 class="panel__title-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h2>
          </div>
          <div class="panel__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog-page3.jpg" class="panel__blog-image">
          </div>
          <div class="panel__special-icons">
            <ul>
              <li class="panel__special-icon-list">
                <i class="fa fa-calendar panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">November 19, 2017</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-align-left panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Blog, Images, Blog, Posts</span
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-user panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Admin</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-comments panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">1 Comments</span>
              </li>
            </ul>
          </div>
          <div class="panel__blog-paragraph">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimectetur adipisicing elit, sed ectetur adipisicing elit, sed </p>
          </div>

          <div class="panel__button">
            <button class="button button--readmore"><span class="panel__button-readmore">READ MORE</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
          </div>
        </div>

        <div class="panel__blog-list">
          <div class="panel__blog-title">
            <h2 class="panel__title-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h2>
          </div>
          <div class="panel__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog-page4.jpg" class="panel__blog-image">
          </div>
          <div class="panel__special-icons">
            <ul>
              <li class="panel__special-icon-list">
                <i class="fa fa-calendar panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">November 19, 2017</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-align-left panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Blog, Images, Blog, Posts</span
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-user panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">Admin</span>
              </li>
              <li class="panel__special-icon-list">
                <i class="fa fa-comments panel__icon" aria-hidden="true"></i>
                <span class="panel__icon-title">1 Comments</span>
              </li>
            </ul>
          </div>
          <div class="panel__blog-paragraph">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimectetur adipisicing elit, sed ectetur adipisicing elit, sed </p>
          </div>

          <div class="panel__button">
            <button class="button button--readmore"><span class="panel__button-readmore">READ MORE</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>

      <aside class="sidebar">
        <div class="sidebar__content">
          <div class="sidebar__title">
            <h2>POST CATEGORY</h2>
          </div>
          <ul class="sidebar__detail">
            <li class="sidebar__list">Lorem Ipsum is simply dummy</li>
            <li class="sidebar__list">Text of the printing and typesetting </li>
            <li class="sidebar__list">Industry. Lorem Ipsum has been </li>
            <li class="sidebar__list">The industry's standard </li>
            <li class="sidebar__list">Dummy text ever since the 1500</li>
            <li class="sidebar__list">When an unknown printer took a </li>
            <li class="sidebar__list">Galley of type and scrambled  </li>
            <li class="sidebar__list">Type specimen book </li>
            <li class="sidebar__list">Lorem Ipsum is simply dummy</li>
            <li class="sidebar__list">Lorem Ipsum is simply dummy</li>
          </ul>
        </div>

        <div class="sidebar__content">
          <div class="sidebar__title">
            <h2>POPULAR POSTS</h2>
          </div>
          <ul class="sidebar__detail">
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar1.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
            <div class="sidebar__image-container">
              <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar2.jpg" alt="" class="sidebar__image">
            </div>
            <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
          </li>
          <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar3.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar4.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar5.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar6.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar7.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
            <li class="sidebar__list">
              <div class="sidebar__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/sidebar8.jpg" alt="" class="sidebar__image">
              </div>
              <span class="sidebar__post-content">Lorem ipsum dolor sit amet, consectetuer adi sed diam nonummy nibh </span>
            </li>
          </ul>
        </div>
        <div class="sidebar__content">
          <div class="sidebar__title">
            <h2>ARCHIVES</h2>
          </div>
          <ul class="sidebar__detail">
            <li class="sidebar__list">October 2016 (4)</li>
            <li class="sidebar__list">September 2016 (3) </li>
            <li class="sidebar__list">June 2017(3)</li>
            <li class="sidebar__list">December 2016 (3)</li>
            <li class="sidebar__list">August 2016 (3)</li>
            <li class="sidebar__list">April 2017 (2)</li>
          </ul>
        </div>

        <div class="sidebar__content">
          <div class="sidebar__title">
            <h2>SOCIAL MEDIA</h2>
          </div>
          <ul class="sidebar__social-icons">
            <li class="sidebar__social-list"><i class="fa fa-facebook-square" aria-hidden="true"></i></li>
            <li class="sidebar__social-list"><i class="fa fa-twitter-square" aria-hidden="true"></i></li>
            <li class="sidebar__social-list"><i class="fa fa-instagram" aria-hidden="true"></i></li>
            <li class="sidebar__social-list"><i class="fa fa-linkedin-square" aria-hidden="true"></i></li>
            <li class="sidebar__social-list"><i class="fa fa-rss-square" aria-hidden="true"></i></li>
            <li class="sidebar__social-list"><i class="fa fa-pinterest-square" aria-hidden="true"></i></li>
          </ul>
        </div>
      </aside>
    </div
  </section>

  <section class="chart">
    <div class="chart__content">
      <div class="chart__information">
        <div class="chart__title">
          <h2>Does you reputation Effecting your Business ?</h2>
        </div>
        <div class="chart__paragraph">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="chart__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
  </section>


  <section class="section blogs">
    <div class="section__content blogs__content">
      <div class="section__title section__title--blogs">
        <h2>Latest Blog</h2>
      </div>  

      <div class="blogs__list-container">
        <ul>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog1.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog2.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog3.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog4.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>


</main>
<?php get_footer(); ?>

