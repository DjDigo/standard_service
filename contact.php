<?php
/**
 * The sidebar containing the main widget area
 * Template Name:contact
 *
 * @package WordPress
 * @subpackage standard_service
 * @since standard_service 1.0
 */

get_header();
?>

<main class="main-content">
  <section class="banner banner--contact">
    <div class="banner__content">
      <h2 class="banner__headline">Contact</h2>
    </div>
  </section>

  <section class="section contact">
    <div class="section__content">
      <div class="section__title section__title--contact">
        <h2>Let's Talk Business</h2>
      </div>

      <div class="contact__content"> 
        <div class="contact__map">
          <div id="map" class="contact__map-content"></div>
        
          <div class="contact__information">
            <div class="contact__list">
              <div class="contact__title">ADDRESS</div>
              <div class="contact__paragraph">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
              </div>
            </div>

            <div class="contact__list">
              <div class="contact__title">EMAIL</div>
              <div class="contact__paragraph">
                <p>jyoung@standardservicellc.com</p>
              </div>
            </div>

            <div class="contact__list">
              <div class="contact__title">PHONE</div>
              <div class="contact__paragraph">
                <p>(716) 982-1016</p>
              </div>
            </div>
          </div>
        </div>

        
        <div class="form">
          <div class="form__title">
            <h2>SEND MESSAGE</h2>
          </div>
          <ul>
            <li class="form__list">
              <input type="text" class="form__input" placeholder="Name">
              <i class="fa fa-user form__icon" aria-hidden="true"></i>
            </li>
            <li class="form__list">
              <input type="email" class="form__input" placeholder="Email">
              <i class="fa fa-envelope form__icon" aria-hidden="true"></i>
            </li>
            <li class="form__list">
              <input type="text" class="form__input" placeholder="Email">
              <i class="fa fa-envelope form__icon" aria-hidden="true"></i>
            </li>
            <li class="form__list">
              <input type="text" class="form__input" placeholder="Which services are you interested in?">
              <i class="fa fa-cogs form__icon" aria-hidden="true"></i>
            </li>
            <li class="form__list">
              <textarea class="form__input form__input--textarea" placeholder="Message"></textarea>
              <i class="fa fa-align-left form__icon" aria-hidden="true"></i>
            </li>
          </ul>
          <div class="form__button">
            <button class="button button--send">SEND</button>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>

<script>
  function initMap() {
    var uluru = {lat: 52.505369, lng: -2.019145};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
    });
  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRECJL-UJtdIHDSRjDhbdeDIpl-Ejzy6g&callback=initMap" async defer></script>



<?php get_footer(); ?>

