<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage ynswebsite
 * @since ynswebsite 1.0
 */
?>
<footer class="footer">
  <div class="footer__container">
    <div class="footer__about-company">
      <div class="footer__title">About Company</div>
      <p class="footer__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
      <p class="footer__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the</p>
    </div>
    <div class="footer__menu">
      <div class="footer__title">Quick Links</div>
      <ul>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Quick Link 1</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Quick Link 2</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Quick Link 3</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Quick Link 4</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Quick Link 5</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="footer__menu">
      <div class="footer__title">Other</div>
      <ul>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Other Link 1</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Other Link 2</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Other Link 3</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Other Link 4</span>
          </a>
        </li>
        <li class="footer__list">
          <a href="" class="footer__link">
            <i class="fa fa-chevron-right footer__link-icon" aria-hidden="true"></i>
            <span class="footer__link-text">Other Link 5</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="footer__menu">
      <div class="footer__title">Contact Us</div>
      <ul>
        <li class="footer__contact-info">
          <a href="" class="footer__contact-link">
            Call: (716) 982-1016
          </a>
        </li>
        <li class="footer__contact-info">
          <a href="" class="footer__contact-link">Email: jyoung@standardservicellc.com</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="footer__copyright-container">
    <div class="footer__copyright">
      <div class="footer__copyright-content">
        <span>Copyright {2017}, All Right Reserved</span>
      </div>
      <div class="footer__copyright-link">
        <a href="" class="footer__copyright-menu">Privacy Policy</a>
        <a href="" class="footer__copyright-menu">Sitemap</a>
      </div>
    </div>
  </div>
</footer>
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
</body>
</html>
