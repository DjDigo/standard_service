<!DOCTYPE html>
<html lang="en">
<head>
  <title>Standard Service LLC</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=100" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/javascript/plugins/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css"/>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascript/jquery-3.1.0.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascript/plugins/slick.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascript/standard.js"></script>
  <?php wp_head(); ?>
  <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <![endif]-->
</head>



<body>
  <header class="header">
    <div class="header__container">
      <div class="header__logo">
        <a href="<?php echo home_url('/'); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo/logo.png" alt="standardservicellc" class="header__logo-image">
        </a> 
      </div>
      <div class="header__icon-menu">
        <div class="header__hamburger-menu">
          <span class="header__hamburger-span"></span>
          <span class="header__hamburger-span"></span>
          <span class="header__hamburger-span"></span>
          <span class="header__hamburger-span"></span>
        </div>
      </div>
      <div class="header__contacts">
        <ul>
          <li class="header__contact-list">
            <a href="" class="header__contact-link">
              <i class="fa fa-envelope-o header__icon" aria-hidden="true"></i>jyoung@standardservicellc.com
            </a>
          </li>
          <li class="header__contact-list">
            <a href="" class="header__contact-link">
              <i class="fa fa-phone header__icon" aria-hidden="true"></i>(716) 982-1016
            </a>
          </li>
          <li class="header__contact-list">
            <button class="button button--lets-talk">
              <span class="button__text">LETS TALK</span>
              <i class="fa fa-chevron-right button__icon" aria-hidden="true"></i>
            </button>
          </li>
          <li class="header__contact-list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/icons/search.png" alt="search-button" class="header__search">
          </li>
        </ul>
      </div>
    </div>
    <div class="header__navigation">
      <div class="header__nav-container">
        <nav class="header__menu">
          <ul>
            <li class="header__navigation-list">
              <a href="<?php echo home_url('/'); ?>" class="header__navigation-link">HOME</a>
            </li>
            <li class="header__navigation-list">
              <a href="<?php echo home_url('/about-us'); ?>" class="header__navigation-link">ABOUT</a>
            </li>
            <li class="header__navigation-list">
              <a href="<?php echo home_url('/services'); ?>" class="header__navigation-link">SERVICES</a>
            </li>
            <li class="header__navigation-list">
              <a href="" class="header__navigation-link">TESTIMONIALS</a>
            </li>
            <li class="header__navigation-list">
              <a href="<?php echo home_url('/blog'); ?>" class="header__navigation-link">BLOG</a>
            </li>
            <li class="header__navigation-list">
              <a href="<?php echo home_url('/contact'); ?>" class="header__navigation-link">CONTACT</a>
            </li>
          </ul>
        </nav>
        <div class="header__social-media">
          <ul>
            <li class="header__social-list">
              <a href="" class="header__social-link">
                <img src="<?php echo get_template_directory_uri(); ?>/images/social/facebook.png" alt="search-button" class="header__social-icon">
              </a>
            </li>
            <li class="header__social-list">
              <a href="" class="header__social-link">
                <img src="<?php echo get_template_directory_uri(); ?>/images/social/twitter.png" alt="search-button" class="header__social-icon">
              </a>
            </li>
            <li class="header__social-list">
              <a href="" class="header__social-link">
                <img src="<?php echo get_template_directory_uri(); ?>/images/social/linkedin.png" alt="search-button" class="header__social-icon">
              </a>
            </li>
            <li class="header__social-list">
              <a href="" class="header__social-link">
                <img src="<?php echo get_template_directory_uri(); ?>/images/social/pinterest.png" alt="search-button" class="header__social-icon">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
