<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage standard_service
 * @since standard_service 1.0
 */

get_header();
?>

<main class="main-content">
  <section class="banner-slider">
    <div class="banner-slider__container">
      <div class="banner-slider__content">
        <h1 class="banner-slider__headline">Reputation Management</h1>
        <h3 class="banner-slider__information">We work internationally with companies, <br/>brands and individuals to deliver <br/>positive and lasting online reputations.</h3>
        <div class="banner-slider__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
   <div class="banner-slider__container banner-slider__container--second">
      <div class="banner-slider__content">
        <h1 class="banner-slider__headline">Search Engine Optimization</h1>
        <h3 class="banner-slider__information">We work internationally with companies, <br/>brands and individuals to deliver <br/>positive and lasting online reputations.</h3>
        <div class="banner-slider__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
    <div class="banner-slider__container banner-slider__container--third">
      <div class="banner-slider__content">
        <h1 class="banner-slider__headline">Fully Custom Solutions</h1>
        <h3 class="banner-slider__information">We work internationally with companies, <br/>brands and individuals to deliver <br/>positive and lasting online reputations.</h3>
        <div class="banner-slider__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
  </section>

  <!-- <section class="banner-slider">
    <ul class="js-slider">
      <li></li>
    </ul>
  </section> -->



  <section class="section about-us">
    <div class="section__content">
      <ul class="about-us__guide">
        <li class="about-us__guide-list">
          <div class="about-us__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/about/about-us-1.png" alt="improving your presence" class="about-us__image">
          </div>
          <span class="about-us__guide-text">IMPROVING YOUR PRESENCE</span>
        </li>
        <li class="about-us__guide-list">
          <div class="about-us__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/about/about-us-2.png" alt="improving your presence" class="about-us__image"> 
          </div>
          <span class="about-us__guide-text">REMOVING NEGATIVE CONTENT</span>
        </li>
        <li class="about-us__guide-list">
          <div class="about-us__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/about/about-us-3.png" alt="improving your presence" class="about-us__image">
          </div>
          <span class="about-us__guide-text">MANAGING YOUR REVIEWS</span>
        </li>
        <li class="about-us__guide-list">
          <div class="about-us__image-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/about/about-us-4.png" alt="improving your presence" class="about-us__image">
          </div>
          <span class="about-us__guide-text">MONITORING AND MEASURING SUCCESS</span>
        </li>
      </ul>

      <div class="about-us__information">
        <div class="section__title section__title--about-us">
          <h2> Lorem Ipsum is simply dummy text </h2>
        </div>
        <div class="about-us__information-container">
          <div class="about-us__information-content">
            <p class="about-us__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            <p class="about-us__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, </p>

            <a href="#" class="about-us__readmore">
              <span class="about-us__readmore-text">read more</span>
              <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
          </div>

          <div class="about-us__video-container">
            <img src="<?php echo get_template_directory_uri(); ?>/images/about/dummy.jpg" alt="improving your presence" class="about-us__video">
          </div>

        </div>
      </div>

    </div>
  </section>


  <section class="section services">
    <div class="section__content services__content">
      <div class="section__title">
        <h2>Our Services</h2>
      </div>

      <div class="services__list-content">
        <ul>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--first"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--second"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--third"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--fourth"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--fifth"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
          <li class="services__list">
            <div class="services__type">
              <span class="services__icon services__icon--first"></span>
              <div class="services__type-title">Lorem Ipsum is simply</div>
              <p class="services__paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the 1500s,</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>

  <section class="testimonial">
    <div class="testimonial__content">
      <div class="testimonial__information">
        <div class="testimonial__title">
          <h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
        </div>

        <div class="testimonial__paragraph">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
        </div>

        <ul>
          <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting industry.</span>
          </li>
          <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing </span>
          </li>
          <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting </span>
          </li>
          <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and t </span>
          </li>
        </ul>
      </div>

      <div class="testimonial__image-container">
        <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/testimonial.jpg" class="testimonial__image" alt="testimonial">
      </div>
    </div>
  </section>


  <section class="chart">
    <div class="chart__content">
      <div class="chart__information">
        <div class="chart__title">
          <h2>Does you reputation Effecting your Business ?</h2>
        </div>
        <div class="chart__paragraph">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="chart__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
  </section>


  <section class="section blogs">
    <div class="section__content blogs__content">
      <div class="section__title section__title--blogs">
        <h2>Latest Blog</h2>
      </div>  

      <div class="blogs__list-container">
        <ul>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog1.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog2.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog3.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog4.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>


</main>


<?php get_footer(); ?>

