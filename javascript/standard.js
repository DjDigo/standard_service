$(document).ready(function(){
	$('.header__hamburger-menu').off().click(function(){
		$(this).toggleClass('header__hamburger-menu--open');
		$('.header__navigation').slideToggle();
	});

	$('.history__accordion').off().click(function(){
    if($(this).find('.history__arrow').hasClass('history__arrow--open') == true ) {
      $(this).find('.history__arrow').removeClass('history__arrow--open');   
    }
    else {
      $(this).find('.history__arrow').removeClass('history__arrow--open');   
      $(this).find('.history__arrow').addClass('history__arrow--open');
    }
    $(this).find('.history__accordion-content').slideToggle();
  })
  
  // for fadein animation
  window.sr = ScrollReveal();
  sr.reveal('.banner__content', { duration: 2000 }, 50);
  sr.reveal('.about-us__guide, .about-us__information', { duration: 2000 }, 50);
  sr.reveal('.services__content', { duration: 2000 }, 50);
  sr.reveal('.testimonial__content', { duration: 2000 }, 50);
  sr.reveal('.chart__content', { duration: 2000 }, 50);
  sr.reveal('.blogs__content', { duration: 2000 }, 50);
  sr.reveal('.contact', { duration: 2000 }, 50);
  sr.reveal('.section__content--panel', { duration: 2000 }, 50);
  sr.reveal('.history', { duration: 2000 }, 50);
  sr.reveal('.history__detail--second, .section__title--second', { duration: 2000 }, 50);


  $('.banner-slider').slick({
    arrows: false,
    dots: true,
    infinite: false,
    speed: 1500,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 1,
  });

});

