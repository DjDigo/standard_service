<?php
/**
 * The sidebar containing the main widget area
 * Template Name:services
 *
 * @package WordPress
 * @subpackage standard_service
 * @since standard_service 1.0
 */

get_header();
?>

<main class="main-content">
<section class="banner banner--services">
  <div class="banner__content">
    <h2 class="banner__headline">Services</h2>
  </div>
</section>

<section class="section testimonial services-page">
  <div class="testimonial__content">

    <div class="section__title section__title--services">
        <h2>Lorem Ipsum is simply dummy text </h2>
    </div>

    <div class="testimonial__paragraph testimonial__paragraph--text">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
    </div>
    
    <div class="testimonial__information">
        <div class="testimonial__title services-page__title">
            <h3 class="services-page__title-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
        </div>

        <div class="testimonial__paragraph">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
        </div>

        <ul>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting industry.</span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing </span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting </span>
        </li>
      </ul>
      <a href="#" class="services-page__readmore">
        <span class="services-page__readmore-text">Read more</span>
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
      </a>
    </div>

    <div class="testimonial__image-container">
        <img src="<?php echo get_template_directory_uri(); ?>/images/services/services1.jpg" class="testimonial__image" alt="testimonial">
    </div>
  </div>

  <div class="testimonial__content testimonial__content--right">
    
    <div class="testimonial__image-container">
      <img src="<?php echo get_template_directory_uri(); ?>/images/services/services2.jpg" class="testimonial__image" alt="testimonial">
    </div>
  
    <div class="testimonial__information">
        <div class="testimonial__title services-page__title">
            <h3 class="services-page__title-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
        </div>

        <div class="testimonial__paragraph">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
        </div>

        <ul>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting industry.</span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing </span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting </span>
        </li>
      </ul>
      <a href="#" class="services-page__readmore">
        <span class="services-page__readmore-text">Read more</span>
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
      </a>
    </div>
  </div>

  <div class="testimonial__content">
    <div class="testimonial__information">
        <div class="testimonial__title services-page__title">
            <h3 class="services-page__title-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
        </div>

        <div class="testimonial__paragraph">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
        </div>

        <ul>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting industry.</span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing </span>
        </li>
        <li class="testimonial__list">
            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial/check.png" alt="testimonial-check" class="testimonial__check">
            <span class="testimonial__text">dummy text of the printing and typesetting </span>
        </li>
      </ul>
      <a href="#" class="services-page__readmore">
        <span class="services-page__readmore-text">Read more</span>
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
      </a>
    </div>

    <div class="testimonial__image-container">
        <img src="<?php echo get_template_directory_uri(); ?>/images/services/services1.jpg" class="testimonial__image" alt="testimonial">
    </div>
  </div>
</section> 

  <section class="chart">
    <div class="chart__content">
      <div class="chart__information">
        <div class="chart__title">
          <h2>Does you reputation Effecting your Business ?</h2>
        </div>
        <div class="chart__paragraph">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="chart__button">
          <button class="button button--banner">
            <span class="button__text">LETS TALK</span>
            <i class="fa fa-chevron-right button__icon button__icon--banner" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </div>
  </section>


  <section class="section blogs">
    <div class="section__content blogs__content">
      <div class="section__title section__title--blogs">
        <h2>Latest Blog</h2>
      </div>  

      <div class="blogs__list-container">
        <ul>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog1.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog2.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog3.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
          <li class="blogs__list">
            <a href="" class="blogs__link">
              <div class="blogs__image-container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/blogs/blog4.jpg" alt="blogs" class="blogs__image">
              </div>
              <div class="blogs__list-content">
                <span class="blogs__list-title">Lorem Ipsum is simply dummy text</span>
                <span class="blogs__list-author">BY RONALD LAWRENCE</span>
                <span class="blogs__list-date">20.01.2017</span>
                <p class="blogs__paragraph">Google will eventually discover if you are breaking the rules. In order to grow your business online</p>
                <span class="blogs__readmore">Read More</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>


</main>
<?php get_footer(); ?>

